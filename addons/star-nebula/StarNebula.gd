tool
extends MeshInstance

class_name StarNebulaDome

#
# ドームの動的移動 + 動的生成雲の設定 by きのもと 結衣 @arlez80
#

export(float) var star_nebula_seed:float = 0.0 setget set_star_nebula_seed

export(Color) var star_color:Color = Color( 1.0, 1.0, 1.0, 1.0 ) setget set_star_color
export(float) var star_aperture:float = 7.0 setget set_star_aperture

export(bool) var auto_follow_camera:bool = true

func _ready( ):
	self.mesh = CubeMesh.new( )
	self.mesh.flip_faces = true
	self._reset_shader_params( )

func set_star_nebula_seed( _star_nebula_seed:float ):
	star_nebula_seed = _star_nebula_seed
	self._reset_shader_params( )

func set_star_aperture( _star_aperture:float ):
	star_aperture = _star_aperture
	self._reset_shader_params( )

func set_star_color( _star_color:Color ):
	star_color = _star_color
	self._reset_shader_params( )

func _reset_shader_params( ):
	var sns:ShaderMaterial = preload( "StarNebulaMat.tres" ).duplicate( )

	sns.set_shader_param( "seed", self.star_nebula_seed )

	sns.set_shader_param( "star_color", self.star_color )
	sns.set_shader_param( "star_aperture", self.star_aperture )

	self.material_override = sns

func _process( delta:float ):
	self._move_to_camera( )

func _physics_process( delta:float ):
	self._move_to_camera( )

func _move_to_camera( ):
	if not self.auto_follow_camera:
		return

	var camera:Camera = self.get_viewport( ).get_camera( )
	if camera == null:
		return

	var middle:float = ( camera.far + camera.near ) / 2.0
	var middle_size:Vector3 = Vector3.ONE * middle

	self.transform.origin = camera.global_transform.origin
	self.transform.basis.x = Vector3( middle, 0.0, 0.0 )
	self.transform.basis.y = Vector3( 0.0, middle, 0.0 )
	self.transform.basis.z = Vector3( 0.0, 0.0, middle )
