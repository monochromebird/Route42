/*
	Star Nebula Shader by Yui Kinomoto @arlez80
	MIT License
*/
shader_type spatial;
render_mode depth_draw_never, cull_back, unshaded, shadows_disabled;

uniform float seed = 0.0;
uniform vec4 star_color : hint_color = vec4( 1.0, 1.0, 1.0, 1.0 );
uniform float star_aperture = 7.0;

float random( vec3 pos )
{ 
	return fract(sin(dot(pos, vec3(12.9898,78.233,53.532532))) * 43758.5453);
}

float value_noise( vec3 pos )
{
	vec3 p = floor( pos );
	vec3 f = fract( pos );

	float v000 = random( p/*+ vec3( 0.0, 0.0, 0.0 )*/ );
	float v100 = random( p + vec3( 1.0, 0.0, 0.0 ) );
	float v010 = random( p + vec3( 0.0, 1.0, 0.0 ) );
	float v110 = random( p + vec3( 1.0, 1.0, 0.0 ) );
	float v001 = random( p + vec3( 0.0, 0.0, 1.0 ) );
	float v101 = random( p + vec3( 1.0, 0.0, 1.0 ) );
	float v011 = random( p + vec3( 0.0, 1.0, 1.0 ) );
	float v111 = random( p + vec3( 1.0, 1.0, 1.0 ) );

	vec3 u = f * f * ( 3.0 - 2.0 * f );

	return mix(
		mix(
			mix( v000, v100, u.x )
		,	mix( v010, v110, u.x )
		,	u.y
		)
	,	mix(
			mix( v001, v101, u.x )
		,	mix( v011, v111, u.x )
		,	u.y
		)
	,	u.z
	);
}

float calc_nebula_value( vec3 v, float aperture )
{
	return clamp(
		pow(
			(
				(
					value_noise( v * 5.45432 ) * 0.5
				+	value_noise( v * 15.754824 ) * 0.25
				+	value_noise( v * 35.4274729 ) * 0.125
				+	value_noise( v * 95.65347829 ) * 0.0625
				+	value_noise( v * 285.528934 ) * 0.03125
				+	value_noise( v * 585.495328 ) * 0.015625
				+	value_noise( v * 880.553426553 ) * 0.0078125
				+	value_noise( v * 2080.5483905843 ) * 0.00390625
				) - 0.5
			) * 2.0 * value_noise( v * 1.365 )
			,	aperture
		)
	,	0.0
	,	1.0
	);
}

void fragment( )
{
	vec3 camera_pos = ( CAMERA_MATRIX * vec4( 0.0, 0.0, 0.0, 1.0 ) ).xyz;

	// カメラ -> 描画する点への単位ベクトル
	vec3 v = -( vec4( VIEW, 1.0 ) * INV_CAMERA_MATRIX ).xyz;
	v.x += seed;

	// 星
	float star_alpha = pow( value_noise( v * 786.54315543 ) * value_noise( v * 452.53254328 ), star_aperture );

	// 星雲
	// 合成
	ALPHA = min(star_alpha, 1.0);
	ALBEDO = star_color.rgb;
	DEPTH = 1.0;
}
