using GodotOnReady.Attributes;
using static Godot.GD;
using System;
using Godot;

public partial class Player : KinematicBody
{
	/* Subnodes */
	[OnReadyGet("HeadPosition")] private Position3D HeadPosition;
	[OnReadyGet("DuckPosition")] private Position3D DuckPosition;
	[OnReadyGet("DuckAnimation")] private Tween DuckAnimation;
	[OnReadyGet("RotatedAxis")] private Spatial RotatedAxis;
	[OnReadyGet("RotatedAxis/Camera")] private Camera Cam;
	[OnReadyGet("Head")] private CollisionShape Head;

	/* Settings */
	[Export] private readonly float Acceleration = 0.1f;
	[Export] private readonly int JumpStrength = 20;
	[Export] private readonly int MaxSpeed = 20;
	[Export] private readonly int Gravity  = 1;

	/* Variables */
	private Vector3 MoveDirection;
	private Vector3 SnapVector;
	private Vector3 Motion;

	private float TargetSpeed = 0;
	private int HeadCount = 0;

	/* Method */
	public override void _PhysicsProcess(float delta)
	{
		MoveDirection.z = Input.GetAxis("forward", "backwards"); MoveDirection.x = Input.GetAxis("left", "right");
		MoveDirection = MoveDirection.Rotated(Vector3.Up, RotatedAxis.Rotation.y).Normalized();

		TargetSpeed = ((MaxSpeed * (IsOnFloor() ? 1 : 2 )) * (Input.IsActionPressed("sprint") && !Head.Disabled ? 2 : (Head.Disabled ? 0.5f : 1)));
		Motion.z = Mathf.Lerp(Motion.z, MoveDirection.z * TargetSpeed, Acceleration / (IsOnFloor() ? 1 : 10));
		Motion.x = Mathf.Lerp(Motion.x, MoveDirection.x * TargetSpeed, Acceleration / (IsOnFloor() ? 1 : 10));

		if(Input.IsActionPressed("duck") || HeadCount > 0)
		{
			if(!Head.Disabled)
			{
				DuckAnimation.InterpolateProperty(Cam, "translation", Cam.Translation, DuckPosition.Translation, 0.1f, Tween.TransitionType.Linear, Tween.EaseType.Out);
				DuckAnimation.Start();
				Head.Disabled = true;
			}
		}
		else if(Head.Disabled)
		{
			DuckAnimation.InterpolateProperty(Cam, "translation", Cam.Translation, HeadPosition.Translation,  0.1f, Tween.TransitionType.Linear, Tween.EaseType.Out);
			DuckAnimation.Start();
			Head.Disabled = false;
		}

		if(IsOnFloor())
		{
			if(Motion.y != -0.1f || SnapVector != Vector3.Down)
			{
				SnapVector = Vector3.Down;
				Motion.y = 0.1f;
			}


			if(Input.IsActionPressed("jump"))
			{
				Motion.y = JumpStrength;
				SnapVector = Vector3.Zero;
			}
		}
		else
		{
			if(SnapVector != Vector3.Zero)
			{
				SnapVector = Vector3.Zero;
			}
			Motion.y -= Gravity;
		}

		Motion = MoveAndSlideWithSnap(Motion, SnapVector, Vector3.Up, true);
	}

	public void _HandleInput(InputEventMouseMotion @event)
	{
		RotatedAxis.Call("_handle_input", @event);
	}

	private void _HeadEntered(Spatial body)
	{
		if(body != this)
		{
			HeadCount += 1;
		}
	}

	private void _HeadExited(Spatial body)
	{
		if(body != this)
		{
			HeadCount -= 1;
		}
	}
}